package br.com.itau.servidorautenticacao;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService{
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	BCryptPasswordEncoder encoder;
	
	public Usuario inserir(Usuario usuario) {
		usuario.setSenha(encoder.encode(usuario.getSenha()));
		return usuarioRepository.save(usuario);
	}
	
	public Optional<Usuario> buscarPorNome(String nome){
		return usuarioRepository.findById(nome);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Usuario> usuarioOptional = buscarPorNome(username);
		
		if(!usuarioOptional.isPresent()) {
			throw new UsernameNotFoundException(username);
		}
		
		Usuario usuario = usuarioOptional.get();
		
		return new User(usuario.getNome(), usuario.getSenha(), Collections.emptyList());
	}
}
